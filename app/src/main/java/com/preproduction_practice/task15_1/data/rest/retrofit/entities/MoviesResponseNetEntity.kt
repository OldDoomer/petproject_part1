package com.preproduction_practice.task15_1.data.rest.retrofit.entities

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class MoviesResponseNetEntity(
    @SerializedName("items") val items: List<Top250DataDetail>,

    @SerializedName("errorMessage") val errorMessage: String
)