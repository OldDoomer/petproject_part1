package com.preproduction_practice.task15_1.data.db.room.dao

import androidx.room.*
import com.preproduction_practice.task15_1.data.db.room.MovieEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDao {

    @Insert
    suspend fun insertAllMovies(vararg movieEntity: MovieEntity)

    @Delete
    suspend fun delete(movieEntity: MovieEntity)

    @Query("SELECT * FROM ${MovieEntity.TABLE_NAME}")
    fun getAllMovies(): Flow<List<MovieEntity>>

    @Transaction
    suspend fun clearAndAddAllMovies(vararg movieEntity: MovieEntity) {
        clear()
        insertAllMovies(*movieEntity)
    }

    @Query("DELETE FROM ${MovieEntity.TABLE_NAME}")
    suspend fun clear()
}
