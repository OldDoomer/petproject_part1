package com.preproduction_practice.task15_1.data.rest.retrofit.entities

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable


@Serializable
data class SearchData(
    @SerializedName("searchType") val searchType: String,
    @SerializedName("expression") val expression: String,
    @SerializedName("results") val results: List<SearchResult>,
    @SerializedName("errorMessage") val errorMessage: String,
)
