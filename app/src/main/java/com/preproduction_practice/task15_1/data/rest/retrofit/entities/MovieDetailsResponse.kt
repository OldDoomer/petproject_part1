package com.preproduction_practice.task15_1.data.rest.retrofit.entities

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class MovieDetailsResponse(
    @SerializedName("title") val title: String,

    @SerializedName("fullTitle") val fullTitle: String,

    @SerializedName("year") val year: String,

    @SerializedName("image") val image: String,

    @SerializedName("plot") val plot: String,
)
