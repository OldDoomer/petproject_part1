package com.preproduction_practice.task15_1.data.rest.retrofit

import com.preproduction_practice.task15_1.data.rest.retrofit.entities.MovieDetailsResponse
import com.preproduction_practice.task15_1.data.rest.retrofit.entities.MoviesResponseNetEntity
import com.preproduction_practice.task15_1.data.rest.retrofit.entities.SearchData
import retrofit2.http.GET
import retrofit2.http.Path

interface InternetApi {

    @GET("/en/API/Top250Movies/$API_KEY")
    suspend fun getTop250Movies(): MoviesResponseNetEntity

    @GET("/en/API/Search/$API_KEY/{expression}")
    suspend fun searchMovie(@Path("expression") searchExpression: String): SearchData

    @GET("/en/API/Title/$API_KEY/{id}")
    suspend fun getMovieById(@Path("id") movieId: String): MovieDetailsResponse

    companion object {
        const val API_KEY = "k_04k3l9ap"
        const val API_KEY_RESERVE_USED = "k_udyt6d0r"
    }
}