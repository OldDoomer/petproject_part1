package com.preproduction_practice.task15_1.data.rest.retrofit.entities

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable


@Serializable
data class Top250DataDetail(
    @SerializedName("id") val id: String,

    @SerializedName("rank") val rank: String,

    @SerializedName("title") val title: String,

    @SerializedName("fullTitle") val fullTitle: String,

    @SerializedName("year") val year: String,

    @SerializedName("image") val image: String,

    @SerializedName("crew") val crew: String,

    @SerializedName("imDbRating") val imDbRating: String,

    @SerializedName("imDbRatingCount") val imDbRatingCount: String,
)
