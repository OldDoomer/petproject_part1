package com.preproduction_practice.task15_1.data.db.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.preproduction_practice.task15_1.data.db.room.dao.MovieDao

@Database(
    entities = [
        MovieEntity::class
    ], version = 1, exportSchema = true
)
abstract class MyDB : RoomDatabase() {
    abstract fun userDao(): MovieDao
}