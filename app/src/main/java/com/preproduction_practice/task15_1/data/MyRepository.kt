package com.preproduction_practice.task15_1.data

import com.preproduction_practice.task15_1.domain.models.MovieDetails
import com.preproduction_practice.task15_1.domain.models.MovieItem
import com.preproduction_practice.task15_1.data.rest.retrofit.InternetApi
import com.preproduction_practice.task15_1.data.db.room.MovieEntity
import com.preproduction_practice.task15_1.data.db.room.dao.MovieDao
import com.preproduction_practice.task15_1.data.rest.retrofit.entities.MovieDetailsResponse
import com.preproduction_practice.task15_1.data.rest.retrofit.entities.MoviesResponseNetEntity
import com.preproduction_practice.task15_1.data.rest.retrofit.entities.SearchData
import kotlinx.coroutines.flow.map
import javax.inject.Inject


class MyRepository @Inject constructor(
    private val movieDao: MovieDao,
    private val remoteDataSource: InternetApi,
    private val logger: Logger
) {

    fun fetchTop250MoviesList() =
        movieDao.getAllMovies().map { entityList ->
            entityList.map {
                MovieItem(it.movieId, it.name, it.subtitle, it.image)
            }
        }

    suspend fun updateMoviesTable() {
        movieDao.clearAndAddAllMovies(*loadTop250MoviesList().toTypedArray())
    }

    // ------------retrofit--------------
    private suspend fun loadTop250MoviesList(): List<MovieEntity> {

        logger.logToast("start loading top 250 movies")
        val movies = convertRemoteDataToMoviesList(
            remoteDataSource
                .getTop250Movies()
        )
        logger.logToast("finish loading top 250 movies")

        return movies
    }

    private fun convertRemoteDataToMoviesList(remoteData: MoviesResponseNetEntity): List<MovieEntity> =
        remoteData.items.map {
            MovieEntity(it.id, it.title, it.fullTitle, it.image)
        }

    suspend fun searchMovie(searchExpression: String): List<MovieItem> =
        convertSearchDataToMoviesList(remoteDataSource.searchMovie(searchExpression))

    private fun convertSearchDataToMoviesList(searchData: SearchData): List<MovieItem> =
        searchData.results.map {
            MovieItem(it.id, it.title, it.description, it.image)
        }

    suspend fun getMovieById(id: String): MovieDetails =
        convertMovieDetailsResponseToMovieDetails(remoteDataSource.getMovieById(id))


    private fun convertMovieDetailsResponseToMovieDetails(r: MovieDetailsResponse): MovieDetails =
        MovieDetails(r.title, r.fullTitle, r.year, r.image, r.plot)

}