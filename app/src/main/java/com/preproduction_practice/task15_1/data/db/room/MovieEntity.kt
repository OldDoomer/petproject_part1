package com.preproduction_practice.task15_1.data.db.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.preproduction_practice.task15_1.data.db.room.MovieEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class MovieEntity(
    @PrimaryKey @ColumnInfo(name = "id")
    val movieId: String,

    val name: String,
    val subtitle: String,
    val image: String
) {
    companion object {
        const val TABLE_NAME = "movies250"
    }
}
