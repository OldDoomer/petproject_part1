package com.preproduction_practice.task15_1.data.rest.retrofit.entities

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable


@Serializable
data class SearchResult(
    @SerializedName("id") val id: String,
    @SerializedName("id") val resultType: String,
    @SerializedName("image") val image: String,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
)

/*
enum class SearchType(val code: Int) {
    Title(1),
    Movie(2),
    Series(4),
    Name(8),
    Episode(16),
    Company(32),
    Keyword(64),
    All(128)
}
*/
