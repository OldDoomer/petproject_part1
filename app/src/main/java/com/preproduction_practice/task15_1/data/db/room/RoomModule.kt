package com.preproduction_practice.task15_1.data.db.room

import android.content.Context
import androidx.room.Room
import com.preproduction_practice.task15_1.data.db.room.dao.MovieDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @Provides
    fun provideRoom(
        @ApplicationContext applicationContext: Context
    ): MyDB {
        return Room.databaseBuilder(
            applicationContext,
            MyDB::class.java, "database-name"
        ).build()
    }

    @Provides
    fun provideMovieDao(
        myDB: MyDB
    ): MovieDao {
        return myDB.userDao()
    }
}