package com.preproduction_practice.task15_1.domain.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.preproduction_practice.task15_1.R
import com.preproduction_practice.task15_1.data.MyRepository
import com.preproduction_practice.task15_1.domain.usecases.UpdateMoviesUseCase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import javax.inject.Inject

@AndroidEntryPoint
class UpdateListService : Service() {
    private var job: Job? = null

    @Inject
    lateinit var updateMoviesUseCase: UpdateMoviesUseCase

    companion object {
        fun startService(context: Context, message: String) {
            val startIntent = Intent(context, UpdateListService::class.java)
            startIntent.putExtra("inputExtra", message)
            ContextCompat.startForegroundService(context, startIntent)
        }

        fun stopService(context: Context) {
            val stopIntent = Intent(context, UpdateListService::class.java)
            context.stopService(stopIntent)
        }

        const val CHANNEL_ID = "UpdateListService"
        const val NOTIFICATION_TITLE = "Data updates in background"
        const val DELAY = 15000L
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //do heavy work on a background thread
        val input = intent?.getStringExtra("inputExtra")
        createNotificationChannel()
        val notificationIntent = Intent(this, UpdateListService::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(NOTIFICATION_TITLE)
            .setContentText(input)
            .setSmallIcon(R.drawable.initial_fragment_title_image_star)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(1, notification)

        startUpdating()

        //stopSelf();
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        stopUpdating()
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun startUpdating() {
        job = CoroutineScope(IO).launch {
            while (true) {
                updateMoviesUseCase.updateMovies()
                delay(DELAY)
            }
        }
    }

    private fun stopUpdating() {
        job?.cancel()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID, "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }
}