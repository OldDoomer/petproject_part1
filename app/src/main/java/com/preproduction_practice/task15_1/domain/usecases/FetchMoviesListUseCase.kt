package com.preproduction_practice.task15_1.domain.usecases

import com.preproduction_practice.task15_1.data.MyRepository
import javax.inject.Inject

class FetchMoviesListUseCase @Inject constructor(private val repository: MyRepository) {
    fun fetchMoviesList() = repository.fetchTop250MoviesList()
}