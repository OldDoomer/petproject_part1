package com.preproduction_practice.task15_1.domain.models

data class MovieItem(
    val movieId: String,
    val name: String,
    val subtitle: String,
    val image: String
)