package com.preproduction_practice.task15_1.domain.usecases

import com.preproduction_practice.task15_1.data.MyRepository
import javax.inject.Inject

class GetMovieByIdUseCase @Inject constructor(private val repository: MyRepository) {
    suspend fun getMovieById(id: String) = repository.getMovieById(id)
}