package com.preproduction_practice.task15_1.domain.models

data class MovieDetails(
    val title: String,
    val fullTitle: String,
    val year: String,
    val image: String,
    val plot: String,
)