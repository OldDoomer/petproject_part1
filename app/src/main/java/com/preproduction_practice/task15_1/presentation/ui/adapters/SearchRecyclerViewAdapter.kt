
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.preproduction_practice.task15_1.databinding.RecyclerViewListItemMovieBinding
import com.preproduction_practice.task15_1.domain.models.MovieItem
import com.squareup.picasso.Picasso

class SearchRecyclerViewAdapter constructor(val itemClickListener: (String) -> Unit) :
    RecyclerView.Adapter<SearchRecyclerViewAdapter.MovieRecyclerViewHolder>() {

    /// individual part for current realization (MVVM)
    private var data: List<MovieItem> = mutableListOf()

    fun updateData(newData: List<MovieItem>) {
        data = newData
        notifyDataSetChanged()
    }

    /// general part
    inner class MovieRecyclerViewHolder(private val binding: RecyclerViewListItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        // here you'll set views in list item

        //binding views to each item (3)
        fun bind(movieItem: MovieItem) {
            binding.listItemName.text = movieItem.name
            binding.listItemSubtitle.text = movieItem.subtitle

            Picasso.get().load(movieItem.image).into(binding.listItemImageView)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieRecyclerViewHolder {
        // here creates first 13 holder one time per life cycle (1)
        val binding =
            RecyclerViewListItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieRecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holderMovie: MovieRecyclerViewHolder, position: Int) {
        // here rebinds each from created holders according scrolling (2)
        holderMovie.bind(data[position])
        holderMovie.itemView.setOnClickListener { itemClickListener(data[position].movieId) }
    }

    override fun getItemCount() = data.size
}