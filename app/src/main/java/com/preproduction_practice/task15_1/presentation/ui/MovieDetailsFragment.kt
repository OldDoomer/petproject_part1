package com.preproduction_practice.task15_1.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.preproduction_practice.task15_1.R
import com.preproduction_practice.task15_1.databinding.FragmentMovieDetailsBinding
import com.preproduction_practice.task15_1.domain.models.MovieDetails
import com.preproduction_practice.task15_1.presentation.viewModels.MovieDetailsViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import java.lang.NullPointerException


@AndroidEntryPoint
class MovieDetailsFragment : Fragment(R.layout.fragment_movie_details) {
    companion object {
        const val ID = "ID"
    }

    private val viewModel: MovieDetailsViewModel by viewModels()
    private lateinit var binding: FragmentMovieDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieDetailsBinding.inflate(inflater, container, false)

        return binding.root
    }

    private fun setObservers() {
        viewModel.movieDetailsFragmentMoviesListLiveData.observe(viewLifecycleOwner) {
            setViews(it)
        }
    }

    private fun setViews(remote: MovieDetails) {
        binding.movieDetailsFragmentTitle.text = remote.title
        binding.movieDetailsFragmentFullTitle.text = remote.fullTitle
        binding.movieDetailsFragmentPlot.text = remote.plot
        binding.movieDetailsFragmentYear.text = "Year: ${remote.year}"

        Picasso.get().load(remote.image).into(binding.imageView)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = requireArguments().getString(ID)

        setObservers()

        try {
            viewModel.getMovieById(id!!)
        } catch (e: IllegalArgumentException) {
            Toast.makeText(context, "illegalArgException\n$e", Toast.LENGTH_SHORT).show()
        } catch (e: NullPointerException) {
            Toast.makeText(context, "nullPtrException (bad id from bundle)\n$e", Toast.LENGTH_SHORT)
                .show()
        } catch (e: Exception) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
        }

    }
}