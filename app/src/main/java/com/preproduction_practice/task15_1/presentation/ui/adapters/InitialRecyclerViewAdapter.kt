package com.preproduction_practice.task15_1.presentation.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.preproduction_practice.task15_1.R
import com.preproduction_practice.task15_1.databinding.RecyclerViewListItemAdvertisementBinding
import com.preproduction_practice.task15_1.databinding.RecyclerViewListItemMovieBinding
import com.preproduction_practice.task15_1.domain.models.MovieItem
import com.squareup.picasso.Picasso
import java.lang.IllegalArgumentException

class InitialRecyclerViewAdapter constructor(val itemClickListener: (String) -> Unit) :
    RecyclerView.Adapter<BaseViewHolder<*>>() {

    /// individual part for current realization (MVVM)
    private var data: List<MovieItem> = mutableListOf()

    fun updateData(newData: List<MovieItem>) {
        data = newData
        notifyDataSetChanged()
    }

    /// general part
    class MovieRecyclerViewHolder(private val binding: RecyclerViewListItemMovieBinding):
        BaseViewHolder<MovieItem>(binding.root) {
        // here you'll set views in list item

        //binding views to each item (3)
        override fun bind(item: MovieItem) {
            binding.listItemName.text = item.name
            binding.listItemSubtitle.text = item.subtitle

            Picasso.get().load(item.image).into(binding.listItemImageView)
        }
    }

    class AdvertisementRecyclerViewHolder(private val binding: RecyclerViewListItemAdvertisementBinding):
        BaseViewHolder<Int>(binding.root) {
        // here you'll set views in list item

        //binding views to each item (3)
        override fun bind(item: Int) {
            binding.listItemImageView.setImageDrawable(
                ResourcesCompat.getDrawable(itemView.resources, item, null)
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val context = parent.context
        return when (viewType){
            MOVIE_TYPE ->
                MovieRecyclerViewHolder(
                    RecyclerViewListItemMovieBinding.inflate(LayoutInflater.from(context), parent, false)
                )
            ADVERTISEMENT_TYPE ->
                AdvertisementRecyclerViewHolder(
                    RecyclerViewListItemAdvertisementBinding.inflate(LayoutInflater.from(context), parent, false)
                )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (((position + 1) % 11) - 1 == 0){ ADVERTISEMENT_TYPE } else { MOVIE_TYPE }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder){
            is MovieRecyclerViewHolder -> {
                holder.bind(data[position])
                holder.itemView.setOnClickListener { itemClickListener(data[position].movieId) }
            }
            is AdvertisementRecyclerViewHolder -> {
                holder.bind(R.drawable.add)
            }
        }
    }


    override fun getItemCount() = data.size

    companion object {
        const val MOVIE_TYPE = 10001
        const val ADVERTISEMENT_TYPE = 10002
    }
}