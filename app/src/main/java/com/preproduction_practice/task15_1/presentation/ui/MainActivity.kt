package com.preproduction_practice.task15_1.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.preproduction_practice.task15_1.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}
