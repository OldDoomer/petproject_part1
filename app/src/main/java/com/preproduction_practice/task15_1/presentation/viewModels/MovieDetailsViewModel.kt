package com.preproduction_practice.task15_1.presentation.viewModels

import androidx.lifecycle.*
import com.preproduction_practice.task15_1.domain.models.MovieDetails
import com.preproduction_practice.task15_1.domain.usecases.GetMovieByIdUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(
    private val getMovieByIdUseCase: GetMovieByIdUseCase,
) : ViewModel() {

    private val detailsScreenResultListLiveData = MutableLiveData<MovieDetails>()
    val movieDetailsFragmentMoviesListLiveData: LiveData<MovieDetails> =
        detailsScreenResultListLiveData

    fun getMovieById(id: String) {
        viewModelScope.launch(Dispatchers.IO) {
            detailsScreenResultListLiveData.postValue(getMovieByIdUseCase.getMovieById(id))
        }
    }

}