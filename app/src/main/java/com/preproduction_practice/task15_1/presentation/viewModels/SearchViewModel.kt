package com.preproduction_practice.task15_1.presentation.viewModels

import androidx.lifecycle.*
import com.preproduction_practice.task15_1.domain.models.MovieItem
import com.preproduction_practice.task15_1.domain.usecases.SearchMovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchMovieUseCase: SearchMovieUseCase
) : ViewModel() {

    private val searchScreenResultListLiveData = MutableLiveData<List<MovieItem>>()
    val searchFragmentMoviesListLiveData: LiveData<List<MovieItem>> = searchScreenResultListLiveData

    fun searchMovie(searchExpression: String) {
        viewModelScope.launch(Dispatchers.IO) {
            searchScreenResultListLiveData.postValue(searchMovieUseCase.searchMovie(searchExpression))
        }
    }

}