package com.preproduction_practice.task15_1.presentation.ui.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T> (itemView: View): RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: T)
}