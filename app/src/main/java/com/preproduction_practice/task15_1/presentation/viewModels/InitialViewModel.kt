package com.preproduction_practice.task15_1.presentation.viewModels

import androidx.lifecycle.*
import com.preproduction_practice.task15_1.domain.models.MovieItem
import com.preproduction_practice.task15_1.domain.usecases.FetchMoviesListUseCase
import com.preproduction_practice.task15_1.domain.usecases.UpdateMoviesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InitialViewModel @Inject constructor(
    private val fetchMoviesListUseCase: FetchMoviesListUseCase,
    private val updateMoviesUseCase: UpdateMoviesUseCase,
) : ViewModel() {

    private val top250MoviesListLiveData =
        fetchMoviesListUseCase.fetchMoviesList().asLiveData(Dispatchers.IO)
    val initialFragmentMoviesListLiveData: LiveData<List<MovieItem>> = top250MoviesListLiveData

    fun updateInitialFragmentMovieList() {
        viewModelScope.launch(Dispatchers.IO) {
            updateMoviesUseCase.updateMovies()
        }
    }
}