package com.preproduction_practice.task15_1.presentation.ui

import SearchRecyclerViewAdapter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.preproduction_practice.task15_1.R
import com.preproduction_practice.task15_1.databinding.FragmentSearchBinding
import com.preproduction_practice.task15_1.presentation.viewModels.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : Fragment(R.layout.fragment_search) {

    private val viewModel: SearchViewModel by viewModels()
    private lateinit var binding: FragmentSearchBinding
    private lateinit var navController: NavController


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        initNavigation()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViews()
        setObservers()

    }

    private fun initNavigation() {
        navController =
            (requireActivity().supportFragmentManager.findFragmentById(R.id.activity_main_navHostFragment) as NavHostFragment).navController
    }

    private fun setObservers() {
        viewModel.searchFragmentMoviesListLiveData.observe(viewLifecycleOwner) {
            (binding.recyclerView.adapter as SearchRecyclerViewAdapter).updateData(it)
        }
    }

    private fun setRecyclerView() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = SearchRecyclerViewAdapter(::recyclerItemClickListener)
        }
    }

    private fun recyclerItemClickListener(id: String) {
        navController.navigate(
            R.id.action_searchFragment_to_movieDetailsFragment,
            bundleOf(MovieDetailsFragment.ID to id)
        )
    }

    private fun setViews() {
        setRecyclerView()

        binding.searchFragmentSearchExpressionEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                binding.searchFragmentSearchButton.isEnabled = p0?.isBlank() == false
            }
        })

        binding.searchFragmentSearchButton.setOnClickListener {
            try {
                viewModel.searchMovie(binding.searchFragmentSearchExpressionEditText.text.toString())
            } catch (e: IllegalArgumentException) {
                Toast.makeText(context, "illegalArgException\n$e", Toast.LENGTH_SHORT).show()
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }
}