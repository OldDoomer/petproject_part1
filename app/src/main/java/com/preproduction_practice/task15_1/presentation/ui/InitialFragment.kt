package com.preproduction_practice.task15_1.presentation.ui

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.preproduction_practice.task15_1.R
import com.preproduction_practice.task15_1.databinding.FragmentInitialBinding
import com.preproduction_practice.task15_1.domain.services.UpdateListService
import com.preproduction_practice.task15_1.presentation.ui.adapters.InitialRecyclerViewAdapter
import com.preproduction_practice.task15_1.presentation.viewModels.InitialViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InitialFragment : Fragment(R.layout.fragment_initial) {

    private val viewModel: InitialViewModel by viewModels()
    private lateinit var binding: FragmentInitialBinding
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInitialBinding.inflate(inflater, container, false)
        initNavigation()


        context?.let { UpdateListService.startService(it, "Foreground Service is running...") }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViews()
        setObservers()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        context?.let { UpdateListService.stopService(it) }
    }
    private fun setObservers() {
        viewModel.initialFragmentMoviesListLiveData.observe(viewLifecycleOwner) {
            (binding.recyclerView.adapter as InitialRecyclerViewAdapter).updateData(it)
        }
    }

    private fun setRecyclerView() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = InitialRecyclerViewAdapter(::recyclerItemClickListener)
        }
    }

    private fun recyclerItemClickListener(id: String) {
        navController.navigate(
            R.id.action_initialFragment_to_movieDetailsFragment,
            bundleOf(MovieDetailsFragment.ID to id)
        )
    }

    private fun initNavigation() {
        navController =
            (requireActivity().supportFragmentManager.findFragmentById(R.id.activity_main_navHostFragment) as NavHostFragment).navController
    }

    private fun setViews() {
        setRecyclerView()

        binding.initialFragmentGoSecondFragmentButton.setOnClickListener {
            navController.navigate(R.id.action_initialFragment_to_searchFragment)
        }

        binding.refreshButton.setOnClickListener {
            startObjectAnimation(it)
            viewModel.updateInitialFragmentMovieList()
        }

        binding.recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL))
        inflateMoviesList()
    }

    private fun startObjectAnimation(v: View){
        with(ObjectAnimator.ofFloat(v, "rotation", 0F, 360F)){
            duration = 400L
            start()
        }
    }

    private fun inflateMoviesList() {
        try {
            viewModel.updateInitialFragmentMovieList()
        } catch (e: IllegalArgumentException) {
            Toast.makeText(context, "illegalArgException\n$e", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

}